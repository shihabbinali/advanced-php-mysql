<?php
// New Word document
include 'msword/PhpWord.php';
echo date('H:i:s'), ' Create new PhpWord object', EOL;

$phpWord = new  PhpWord();
$phpWord->addTitleStyle(1, array('size' => 14, 'bold' => true), array('keepNext' => true, 'spaceBefore' => 240));
$phpWord->addTitleStyle(2, array('size' => 14, 'bold' => true), array('keepNext' => true, 'spaceBefore' => 240));
// 2D charts
//$section = $phpWord->addSection();
// Add style definitions
$phpWord->addParagraphStyle('pStyle', array('spacing' => 100));
$phpWord->addFontStyle('BoldText', array('bold' => true));
$phpWord->addFontStyle('ColoredText', array('color' => 'FF8080'));
$phpWord->addLinkStyle(
    'NLink',
    array('color' => '0000FF', 'underline' => \PhpOffice\PhpWord\Style\Font::UNDERLINE_SINGLE)
);
// Add text elements
// Save file
echo write($phpWord, basename(__FILE__, '.php'), $writers);