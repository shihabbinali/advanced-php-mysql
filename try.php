<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class MyException extends Exception{};
class MyNegativeException extends Exception{};

$a = 10;
$b = 2;

try {
    
    if($b > 0) {
    $c = $a/$b;
    
    echo $c;
    } else {
        if($b == 0)
            throw new MyException("Can not divide by 0");
        if($b < 0)
            throw new MyNegativeException("Please provide positive integer only");
    }
    
    echo "I am done with division";
    
} catch (MyException $ex) {
    //echo "Something happened!!";
    echo "From custom Exception message: ".$ex->getMessage();
    
    //print_r($ex);
} catch (MyNegativeException $ex) {
    //echo "Something happened!!";
    echo "From Negative message: ".$ex->getMessage();
    
    //print_r($ex);
} catch (Exception $ex) {
    //echo "Something happened!!";
    echo $ex->getMessage();
    
    //print_r($ex);
} finally {
    echo " <Br />i am here finally <br />";
}

echo "done with all ajaira thing.<br />";

try {
    
    try {
        throw new MyException("I am from inside");
    } catch (Exception $ex) {
        echo $ex->getMessage();
    }
    
    echo "I am inside try.<br />";
    
    
    
} catch (MyException $ex) {
    echo "My Exp ".$ex->getMessage();
} catch (Exception $ex) {
    echo "outside ".$ex->getMessage();
}


set_exception_handler('myException');

function myException() {
    echo "I am my exception";
}

throw new BadMethodCallException;