<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class cricketer {
    public $name = "";
    public $run = "";
    public $level = "";
    private $phoneNumber = "";
    protected $email = "";
    
    
    function __construct ($name= '', $run = 0, $level = 0, $phoneNumber = "", $email = "") {
        $this->run = $run;
        $this->name = $name;
        $this->level = $level;
        $this->phoneNumber = $phoneNumber;
        $this->email = $email;

    } 
    


    function getPhoneNumber() {
        return $this->phoneNumber;
    }
    
    function getName() {
        return $this->name;
    }
    
    function setName($val = "") {
        $this->name = $val;
    }
    
    function getRun() {
        return $this->run;
    }
    
    function setRun($val = "") {
        $this->run = $val;
    }
    
    function getLevel() {
        return $this->level;
    }
    
    private function setLevel($val = "") {
        $this->level = $val;
    }
}

$sakib = new cricketer("Sakib Al Hasan", 8888, "International Player", 123456789, "abc@tigercricket.com");
$sakib->phoneNumber = 1234;
echo "Name is ".$sakib->getPhoneNumber();

$account = new Account();
$account->savingsRate = 99.99;