<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$currentNumber = '1101111000';
$currentBase = 2;
$targetBase = 10;

$chars = range(0,9);
$chars[] = 'A';
$chars[] = 'B';
$chars[] = 'C';
$chars[] = 'D';
$chars[] = 'E';
$chars[] = 'F';
$keys = ['0' => 0, '1' => 1,'2' => 2 ,'3' => 3,'4' => 4, '5' => 5, '6' => 6, '7' => 7, '8' => 8, '9' => 9, 'A' => 10, 'B'=> 11, 'C' => 12, 'D' => 13, 'E' => 14, 'F' => 15];


function convertToDecimal($baseString, $currentBase) {
    global $keys;
    $decimalNum = 0;
    $baseString = strrev($baseString);
    for($i = 0; $i<strlen($baseString);$i++) {
        $ch = substr($baseString, $i, 1);
        $digit = $keys[$ch];       
        //echo "digit > $ch > ".$digit."<Br />";
        $decimalNum += (($currentBase**$i)*$digit);
    }
    
    return $decimalNum;
}

function convertToAnyBase($decimalNumber, $targetBase) {
    global $chars;
    $convertedString = "";
    
    do {
        $mod = $decimalNumber%$targetBase;
        $convertedString = $chars[$mod] . $convertedString;
        $decimalNumber = (int) ($decimalNumber/$targetBase);
    } while ($decimalNumber);
    
    return $convertedString;
}

if($currentBase == 10) {
echo convertToAnyBase($currentNumber, $targetBase);
} else {
    $newNumber = convertToDecimal($currentNumber, $currentBase);
    //echo $newNumber;
    echo convertToAnyBase($newNumber, $targetBase);
}