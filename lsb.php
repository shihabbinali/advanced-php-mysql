<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Cricketer {
    public static function iAmWho() {
        echo "Cricketer";
    }
    
    public static function nameMe() {
        static::iAmWho();
    }
}

class NationalCricketer extends Cricketer{
    
    public static function iAmWho() {
        echo "National Cricketer";
    }
}

NationalCricketer::nameMe();
Cricketer::nameME();

