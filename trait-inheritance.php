<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

trait batsmen {
    
    function canBat() {
        parent::canBat();
        echo "yes I can bat";
    }
    
    function canBowl() {
        
    }
    
}

trait bowler {
    function canBowl() {
        echo "Yes i am a bowler as well";
    }
    
    function canBat() {
        echo " i am hard hitting tailender";
    }
}

trait wicketkeeper {
    public function canKeep() {
        echo "Yes i am keeper";
    }
}

trait nationalCricketer {
    use batsmen,  wicketkeeper;
    
    function sayHello() {
        
    }
}

class Cricketer {
    
    function canBat() {
        echo " A cricketer can bat";
    }
    
    
}


class InternationalCricketer extends Cricketer {
    
    
    use batsmen, bowler, wicketkeeper {
    
        batsmen::canBat insteadof bowler;
        bowler::canBat as allRounderBat;
        bowler::canBowl insteadof batsmen;
        wicketkeeper::canKeep as private canWicketKeep;
    }
    
}

$sakib = new InternationalCricketer;
$sakib->allRounderBat();
$sakib->canWicketKeep();