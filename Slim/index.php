<?php

require 'vendor/autoload.php';
require '../db.php';

$app = new Slim\App();




$app->post('/api/hello/{name}/{type}', function ($request, $response, $args) use($pdo) {
    
    $result = $pdo->query("select * from students");

    $names = [];
    foreach ($result as $row) {
        $names[] = ["name" => $row['middleName']];
    }    
    $newResponse = $response->withStatus(401);    
    //$newResponse = $response->withHeader('Content-type', 'application/json');
    switch(args['type']) {
        
        case 'csv':        
        case 'txt':
            $newResponse->write(implode(",",$names));
        break;
    
        case 'xml':
            $newResponse->write(array_to_xml($names));
        break;
    
        case 'json':
            $newResponse->write(json_encode($names));
        break;
    
        default:
            $newResponse->write(json_encode($names));
        
    }
    //$newResponse->write(json_encode($names));
    
    //$response->write("Hello, " . $args['name']);
    return $newResponse;
});

$app->post("/savename", function ($request, $response, $args) {
    $response->write("<book><name>mastering php 7</name><author>Hasin hyder</author></book>");
    return $response;
});


$app->get('/', function ($request, $response, $args) {
    $response->write("Welcome to home page - mizan ");
    return $response;
});

$app->run();
